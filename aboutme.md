---
layout: page
title: About Me
subtitle: Why this blog
---

My name is Davinder & following are the reasons I have set this blog up

- I wanted to put my textual posts somewhere other than my main website [DavChana.com](https://davchana.com), which is primarily copy of my [Instagram](https://instagram.com/davchana) page, & thus a photo-heavy website.
- I wanted to try something else that Wordpress, so came across static website generators, & with lovely GitLab.com pages + Jekyll, I managed to bring up this simple, easy, light-on-resources, dependable blog.

What else do you need?

### my history

* [ ] todo
