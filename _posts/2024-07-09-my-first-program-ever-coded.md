---
layout: post
title: My first program ever coded
tags: [random, code, bio]
---

This post is about one of my first encounters with programming, logic, variables & related.

I came across my first computer in a computer training institute with a name I forgot, but it was a franchise chain in India, in 1999 Summer. It was a white CRT monitor with white mouse & keyboard, course was for Rs 1,500 for 20 days, with a focus on Notepad, Mouse Usage, DOS Commands & MS Paint. First few hours were on a whiteboard & then the practical time on an actual Computer for about 30 minutes a day.

Then I got a lot of computing time access in my college, in Internet Lab, & a friend of mine also bought a PC, where I used to go on weekends or vacation. In college I was introduced to C & C++, & basic logics, pointer, cursor, compiler, errors & stuff. There on October 30th, 2003, a Friday, I wrote a simple C program to itch my own scratch & not as any class assignment. I 
proudly named it age/_find.c

It was a simple C program, which, when compiled, gives an execution file .exe, running as a command prompt, asking for two dates. I was so much proud of its internal logic of figuring out which date is older & which is newer & which one is older.

First lines were simply declaring the header files for Standard Input Output (Keyboard, Monitor etc) & console Input/Output:    

    #include<stdio.h>
    #include<conio.h>

Then the main() function & integers:

    void main ()
    {
    int d1,m1,y1,d2,m2,y2,d,m,y;

d1, m1, y1 being the newer date's day, month & year; d2, m2, y2 the sames of second date. d,m,y being the difference. Clever variable names.

Then a comment block with proper vertical aligned text:

    /*Programmed by Davinder Singh
     *DS Inc.
     *30th October,2003
     *CAD CAM Lab, SLIET*/

Asking & assigning the inputs, with code comments:

    printf("Enter first date(dd mm yyyy)\t");
    scanf("%d%d%d",&d1,&m1,&y1);              /*The first date input.*/
    
    printf("Enter second date(dd mm yyyy)\t");
    scanf("%d%d%d",&d2,&m2,&y2);             /*The second date input.*/

The if logic to detect if valid dates were entered (a date like 32 11 2013 would be invalid because there is no 32 day or 13th month).

    if (d1<=31&&d2<=31&&m1<=12&&m2<=12)      /*The wrong date input encountered.*/
    	{

now the logic to detect which date is newer, & to find the difference (~~yeah, a bug, the logic does not check if same year but different months or days). In defence, the program is age_find aka age finder, and only a kid born in the current year will encounter that bug  e.g. 15 08 2024 & 19 12 2024~~ Not a bug, accounted for it in next few lines.

    if (y1>=y2)                           /*The latest date recognised.*/
    		{
    		y=y1-y2;
    		m=m1-m2;
    		d=d1-d2;
    		}
& if dates entered other way around

    else
    		{
    		y=y2-y1;
    		m=m2-m1;
    		d=d2-d1;
    		}

If month and or day is 0 or negative, subtract 1 from year & add numbers to month and day. E.g. between 15 08 2024 & 19 12 2020, y would be 2024-2020=4, month would be 8-12 = -4. Now this logic will add 12 to month making it 12+(-4)=8 & subtracting 1 from year making it 3, total 3 years & 8 months. Same with day. it simply assumes every month as of 30 days.

    if (m<=0)
    	{
    	y=y-1;
    	m=12+m;
    	}
    if (d<=0)
    	{
    	m=m-1;
    	d=30+d;
    	}
Now, to print the results in human readable format on console, output as

> The diffrence between date
> 15-08-2024 and
> 19-12-2020 is
> 3 years, 7 months, 26 days.

	printf("The diffrence between date\n%d-%d-%d and\n%d-%d-%d is\n%d years, %d months, %d days.",d1,m1,y1,d2,m2,y2,y,m,d);
	}
	
If wrong dates were entered at runtime, this else block says:

    else
    {
    printf("Oops! The dates entered are invalid."); /*The messsage for wrong date input.*/
    }

& final curly bracket to close the function:

    }

Full code:

    #include<stdio.h>
    #include<conio.h>
    void main ()
    {
    int d1,m1,y1,d2,m2,y2,d,m,y;
    /*Programmed by Davinder Singh
     *DS Inc.
     *30th October,2003
     *CAD CAM Lab, SLIET*/
    printf("Enter first date(dd mm yyyy)\t");
    scanf("%d%d%d",&d1,&m1,&y1);              /*The first date input.*/
    printf("Enter second date(dd mm yyyy)\t");
    scanf("%d%d%d",&d2,&m2,&y2);             /*The second date input.*/
    if (d1<=31&&d2<=31&&m1<=12&&m2<=12)      /*The wrong date input encountered.*/
    	{
    	if (y1>=y2)                           /*The latest date recognised.*/
    		{
    		y=y1-y2;
    		m=m1-m2;
    		d=d1-d2;
    		}
    	else
    		{
    		y=y2-y1;
    		m=m2-m1;
    		d=d2-d1;
    		}
    	if (m<=0)
    		{
    		y=y-1;
    		m=12+m;
    		}
    	if (d<=0)
    		{
    		m=m-1;
    		d=30+d;
    		}
    	printf("The diffrence between date\n%d-%d-%d and\n%d-%d-%d is\n%d years, %d months, %d days.",d1,m1,y1,d2,m2,y2,y,m,d);
    	}
    else
    	{
    	printf("Oops! The dates entered are invalid."); /*The messsage for wrong date input.*/
    	}
    }
