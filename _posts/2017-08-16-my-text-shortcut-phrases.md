---
layout: post
title: My Text Shortcut Phrases
tags: [random, hack, information, wiki, productive]
---

*This is the post about the text shortcut phrases I use on android phones.*

###  Introduction 
We all know how much inconvenient it is to type long repetitive words/excerpts on tiny mobile keyboards. Luckily, there are few ways to use technology for our convenience (as almost every time).

There are certain apps & ways that “expand” what you type to what you actually wanted to type; kind of like shortcuts e.g. typing `ty` will expand to `thank you` & such.

Also, the app can assign some dynamic values like current date, hour, minute, day, month, year in number/word formats. It can also detect the case of shortcut first letter & expand the phrase first letter to same case i.e. `ty` will expand to `thank you`; & `Ty` will expand to `Thank you`

### Tools/Tech
* Devices:
   * Huawei Honor7 with Android
   * Lenovo A6010 with Android
* Application
   * Textpand Pro - *[Play Store Link]( https://play.google.com/store/apps/details?id=com.isaiasmatewos.texpandpro&hl=en)*

### Phrase List

#### key:
**Shortcut**  - Phrase - *Remarks*

1.  **?.**- (?) - *to imply that if what I say/ask is true e.g. WiFi down (?) is it?*
1.  **andor**- and/or
1.  **bjd**- dd/mm - *Bullet Journal Done*
1.  **bjt**-mm/dd ddd: - *Bullet Journal Today*
1.  **btw**- by the way 
1.  **coz**- because
1.  **ctn**- Can't talk now, will call back, Sorry!
1.  **dcm**- DavChana.com - *My Photo Website*
1.  **dcn**- Dav Chana - *My Full Name*
1.  **dcr**- davxxxxxx<span></span>@xxxxx.coxxxx - *My Work Email*
1.  **dfb**- http:<span></span>//davchana.com/fb - *My Facebook (now defunct)*
1.  **dgm**- davxxxxxx<span></span>@gmail.com - *My Google Email*
1.  **dgp**- http:<span></span>//davchana.com/gp - *My Google+ Profile*
1.  **digm**- http:<span></span>//davchana.com/ig - *My Instagram Profile*
1.  **dint**- din’t - *need not to go to symbols section of keyboard*
1.  **dont**- don’t - *same as above*
1.  **dsin**- Davxxxxxx Xxxxgxxx - *My Full Name*
1.  **dtw**- http: <span></span>//davchana.com/tw - *My Twitter Profile*
1.  **e.bear**- ʕ•ᴥ•ʔ - *e stands for emoji*
1.  **e.lenny**- ( ͡° ͜ʖ ͡°) - *the famous lenny face*
1.  **e.shrug**- ¯\\_(ツ)_/¯ - *shrugs*
1.  **fmin**- I will be there in five minutes..
1.  **gmg**- Good Morning!!
1.  **gmh**- Good Morning, How are you?
1.  **hbda**- Many many happy returns of the day to you! Here's wishing you a day filled with fun and delight!!  - *Happy Birthday A*
1.  **hbds**- Happy Birthday!! Enjoy - *Happy Birthday Short*
1.  **hru**- How are you?
1.  **hsf**- Have a Safe Flight!! Enjoy - *have safe flight*
1.  **htps**- https:// - *again, no need to go symbols section of keyboard*
1.  **igc**- #Canon #60D #18135mm #DavChana - *Instagram Tags for Canon Camera*
1.  **igf**- #Fuji #FujiFilm #XA1 #1655mm #DavChana - *Instagram Tags for Fuji Camera*
1.  **ighw**- #Huawei #Honor7 #Phone #DavChana - *IG Tags for Huawei Phone, igh comes in many words like hIGH, nIGHt etc.*
1.  **igs**- #Sony #alpha #a5000 #1855mm #DavChana - *IG Tags for Sony*
1.  **igtags**- \n\n#TravelGram #ThroughMyEyes #Travelling #Europe #2015 #GoodTimes #TravelBug #Moments #Live #Breathtaking #Beauty #Life #InstaGood #InstaLike - *Quick Instagram Tags*
1.  **lbb**- I Love You!!
1.  **mpl**- My pleasure
1.  **nws**- No Worries:)
1.  **ok**- okay
1.  **omg**- Oh my God!
1.  **otw**- on the way
1.  **plz**- please
1.  **ppl**- people
1.  **ssa**- Sat Sri Akal.. - *Way of Greeting in Punjabi  - [Wikipedia]( https://en.wikipedia.org/wiki/Sat_Sri_Akaal)*
1.  **tcb**- take care, bye!
1.  **thts**- that’s
1.  **thx**- Thanks
1.  **ty.**- thank you.. - *dot is required, because ty comes in many common words like naughty; & dot is on the same screen as alphabets on keyboard*
1.  **tyvm**- Thank you very much.. - *initials of the phrase*
1.  **vsrc**- view-source: - *to quickly view the source in Google Chrome Android, click in URL bar start, type this*
1.  **whod**- https:<span></span>//who.is/dns/ - *to quickly view the DNS records of current domain in Google Chrome Android*
1.  **whow**- https:<span></span>//who.is/whois/ - *to quickly view the WHOIS records of current domain in Google Chrome Android*
1.  **wru**- Where are you?
1.  **yawc**- You are Welcome

Saved around 69K+ characters by using this.