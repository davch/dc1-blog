---
layout: post
title: My Journey with Domains
tags: [random, domains, bio, exciting-stuff]
---

*This is the post about my experiences with Domain Names & related.*

My first domain purchase was **davinder.net** in November 2009, & from there until now I have spent about INR *~43,000/-* on domains, with various registrars.

I still remember the moment I read a square advertisement in paper newspaper at Chandigarh (Punjab) about domains & web hosting. I called the phone numbers listed, but nobody answered. Then I used Google Maps (on Nokia) to walk to the address, found that the ground floor is just a cloth-shop, & the actual IT shop is on first floor. Went up there, met this guy (owner/operator of BagFull.net, Jagroop). Asked him that I want a domain for my name, he searched, found that .net version is available, & will cost me INR 450. I paid in cash, he made me an account on bagfull.net, gave me password (I already had Gmail email account before that), & thus I became proud owner of davinder.net.

I moved back to Kenya for work, & then found this JLKHosting.com free cPanel Hosting, & started using it for simple pages (I do not have any idea what I hosted on it there on those days). Signed up for Google Apps email, & had emails like d@ etc.

As I look more on my Domains Expenses Database spread sheet (yes, I have one :)), in next two years, I renewed this domain at same registrar for same price, & no other. Somewhere in June 2011 I registered the .org version of my name; & dropped it next year.

Then I found mitsu.in in October 2011, & registered lot of .in domain, like davindr.in (davinder.in was not available), ds1.in, harw.in, vindr.in (for d@vindr.in), channa & chana, idav, xsy, & bunch others because of on-going promo of INR 79 per domain for registration. Most of them were dropped next year, few of them stayed with me till today. First transaction with GoDaddy was in March 2012 & total 115 transactions counting new registrations, renewals, transfers & such with multiple registrars.

After that came across namepros, inforum, namecheap, php, gitlab, hackernews, yourls, reddit, google apps, logo, yandex, zoho & lot of other such related.

Now, after almost **7 Years & 7 Months**, with 54 unique domains, with average expense of INR 16 per day, I have now only 15 domains I care about (4 of these 15 are on anvil for not-renewal). The furthest expiry date of any of my domains is in 2020, & TLDs like .com, .in, .net, .ninja, .xyz & .cm are part of those 15.

I have learned a lot about technical sides, subdomains, CNAMEs, MX, A, propagation, & still find myself reading or searching for related issues.

Still looking for one more domain to purchase :)