---
layout: post
title: How to fix FNU or NONAMEGIVEN on Work Permit or Green Card
tags: [random, immigration, documents]
---

If you are in USA, your name is FIRST, MIDDLE, LAST & your government documents has your name in the form of NONAMEGIVEN FIRST, or FNU LAST, or LAST LAST, or UNKNOWN FIRST, continue reading this post to find more.

**The Problem**

*Note that Passport means INDIAN PASSPORT. *All of this is just ramblings, not any legal advice, do everything after you understand, only you are responsible for anything & everything about your documents. *

All of your USA documents are based on your Birth Document which is your Indian Passport. It does not matter if you think your first name is ARJUN, if it is written in Surname section in Indian Passport, it is SURNAME (or Last Name). Look in your passport, if GIVEN NAME section is empty & that if your FIRST LAST is written only in SURNAME section. If GIVEN NAME is empty, your USA Documents like Visa, Green Card, Work Permit, Employment Authorization Card etc may have first name as FNU or NONAMEGIVEN or something similar, as this section can not be left blank.

**The Solution**

The ideal scenario is to get passport with proper name split before you get any USA documents i.e. FIRST in GIVEN NAME; & LAST in SURNAME. That is before you apply any USA application in India. If you are attending the interview at Mumbai Consulate, you can try requesting your visa officer that this is my LAST name & this is my FIRST name, but there is no guarantee.

If you are past that stage, & already have EAD or GREEN CARD with these Name Versions, now you have two paths. Having an EAD or Green Card with this format of name means as of now you have to use the exact name in DMV or Social Security or anything official paperwork.

First path is to embrace this exact name, & change it when you file for citizenship N400 as name-change. That is easy but takes 5+ years.

Second more labor intensive path is, to get Indian Passport with Split Name. Google for your city's closest Indian Consulate to find out how to apply for Passport with Split Name. You need to fill some government forms, gather some proofs, & mail or visit VFS with everything with fees. In about 6 weeks or more you may get your corrected passport. Now send this to USCIS to update your document, which may cost at least $500 or more & may take a year or two or more. Once you have your card corrected, now you may approach DMV & Social Security; & then your employer, banks, & etc.

Good Luck!!