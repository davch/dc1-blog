---
layout: page
title: Now
subtitle: "What's up?"
permalink: /now/
---

*This is a [/now](https://nownownow.com/about) page and if you have your own site, you should make one, too. It only talks about now, not what’s next :)*

**Last Updated 2023-11-11 Saturday 10:45pm**

*First update of 2023* Here’s what I’m working on now / going on in my life, in no particular order:

1. Worked with javascript in PDFs with Fox-IT PDF Editor & Forms & logics.
1. Restarted using Bullet Journal (not pretty, just a to-do list).
1. Travlled a lot.
1. May rewatch Big Bang Theory.
1. One phone has *only* 67 tabs. Other one has D:) number.

### Older Updates

**2022-12-01 Thursday 09:43pm**

1. Soon after my last update I got the fulltime version of the same job, so full 40 hours a week.
1. Rested 3 days on Thanksgiving Weekend, Sunday I went to Gurudwara Sahib.
1. Planning a photo walk trip to San Franscisco, winter christmas theme.
1. I have started again using a paper bullet journal, listing all of the regular to-dos & stuff.
1. Only a week ago my main phone again got 100+ tabs, but I am trying to bring it down to 99 or less.
1. Keeping up with Young Sheldon.

**2022-02-18 Friday 01:32am**

1. I am in California, working in a comfortable job; although occasionally not getting full 40 hours per week; & thus trying to do some gigs alongside.
1. Had a week long vacation after so many months; almost first time since I came to US.
1. Have lots of credit cards, for different purposes like gas, rent, food, grocery, online & such; but no carry over balance or interest.
1. As my work now is not on one desk; I struggle to use paper bullet journal; & I also am not keeping up with tracking my daily expenses.
1. I have taken this as a fact that my devices will always have 100+ tabs.
1. Keeping up with Young Sheldon, waiting for Better Call Saul.
1. A lost list of pending books to read & movies to watch & places to visit.

**2019-03-07 Thursday**

1. I am in California, working odd jobs & trying to make ends meet.
1. For few weeks my both phones had >100 tabs.
1. I have learned a lot about Google Apps Script & related.
1. I am still struggling to learn about Miles/Gallons/MM-DD/Pints etc.

**2017-08-16 Wednesday**

1. I am in Doha, Qatar, working with Carillion Qatar.
1. I am working on being a good Husband.
1. One of my Phone has **:D** number of tabs open, most of them from hacker news or links from there, all either I am supposed to read; or process/note-down/learn. The other phone has **51**+ tabs, all I need to read/note/process.
1. I have come off from Facebook, as it was consuming too much time.
1. I am learning Jekyll & Liquid. Also I learned & started writing [personal library](https://gitlab.com/davch/cdn/blob/master/public/common.js) for frequently used javascript functions.
1. I am enjoying the TV Show [Game of Thrones Seson 07](https://en.wikipedia.org/wiki/Game_of_Thrones_(season_7)), & planning to read the books to sometime soon in future.
1. I have also molded Google Keep as my temporary on-the-go [Bullet Journal](http://bulletjournal.com/get-started/), & is working fine.
1. I am trying to prioritize My Health, Presence with Family as compared to work.
1. Want to resume posting photos on [Instagram](http://davchana.com/ig), & also to start using the camera again.
